package stockage;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import main.Solution;

public class Liste implements ISolution{
	protected String nom;
	protected int nbSolutions;
	protected List<Solution> solutions;

	public Liste() {
		solutions = new ArrayList<Solution>();
		nbSolutions = 0;
		nom = "liste";
	}
	public void afficher() {
		System.out.println("Affichage "+nom+" :");
		for(Solution sol : solutions) {
			sol.afficher();
		}
	}
	protected void ajouter(int pos, Solution solution) {
		boolean[] tab = solution.table;
		solution.table = Arrays.copyOf(tab, tab.length);
		solutions.add(pos, solution);
	}
	public int nombreSolutions() {
		return nbSolutions;
	}

	public void ajouterSolution(Solution solution) {

		System.out.print("ajout ");solution.afficher();
		if(!contientDeja(solution))	{
			ajouter(0, solution);
			//solutions.add(solution);
			nbSolutions++;
		}
	}

	public boolean contientDeja(Solution solution) {
		for(Solution sol : solutions) 
			if(solution.compareTo(sol) == 0)
				return true;
		return false;
	}
}
