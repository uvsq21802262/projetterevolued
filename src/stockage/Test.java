package stockage;

import main.Terme;

public class Test {

	public static void main(String[] args) {
		
		//si aleatoire = true alors les variables apparaitront dans un ordre aleatoire sinon dans l'ordre
		boolean aleatoire = true;
		//nb de variables differentes dans la formule
		int nbVar = 3;
		//nb de termes maximum dans la formule
		int nbMaxTermes = 3;
		//taille minimum du terme ( en variables )
		int tailleMinTerme = 1;
		//proba d'aparition qu"un variable soit negative
		double probaNot = 0.3;
		//proba d'aparition qu"une variable apparraisse dans chaque terme
		double probaVar = 0.5;
				
		Terme.grayStrategy strat = Terme.grayStrategy.ITER1;
		// Les solution possilbles
		ISolution sol1;
		ISolution sol2;
		ISolution sol3;
		ISolution sol4;
		ISolution sol5;
		
		sol1= new ABR();
		sol2 = new HMap(32);
		sol3 = new Liste();
		sol4 = new ListeTriee();
		sol5 = new Trie(nbVar);
		
		Testeur test = new Testeur(true, nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, strat, sol1, true);
		System.out.println("-----------------TEST 1--------------------");
		System.out.println("-----------------Arbre de Recherche--------");
		test.Tester();
		
		System.out.println("-----------------TEST 2--------------------");
		System.out.println("-----------------Hachage-------------------");
		test.setSol(sol2);
		test.Tester();
		
		System.out.println("-----------------TEST 3--------------------");
		System.out.println("-----------------LISTES--------------------");
		test.setSol(sol3);
		test.Tester();
		
		System.out.println("-----------------TEST 4--------------------");
		System.out.println("-----------------LISTE TRIEE---------------");
		test.setSol(sol4);
		test.Tester();
		
		System.out.println("-----------------TEST 5--------------------");
		System.out.println("-----------------TRIE----------------------");
		test.setSol(sol5);
		test.Tester();
		
		

		
		
	}

}
