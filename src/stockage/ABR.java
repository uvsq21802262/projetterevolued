package stockage;

import java.util.Arrays;

import main.Solution;

//enfant droite sup enfant gauche inf
public class ABR implements ISolution{
	NoeudABR racine;
	int nbSolutions;
	final static int BINARY_SIZE = 2;
	
	static class NoeudABR  { 
		NoeudABR[] children = new NoeudABR[BINARY_SIZE]; 
		Solution valeur; 
		NoeudABR(Solution solution){ 
            for (int i = 0; i < BINARY_SIZE; i++) 
                children[i] = null; 
            this.valeur = solution;
        } 
    }; 
	
    public ABR() {
    	racine = null;
    }
	@Override
	public void afficher() {
		System.out.println("Affichage ABR :");

		if(racine != null)
			afficherRec(racine);
		
	}

	private static void afficherRec(NoeudABR noeud) {
		if (noeud.children[0] != null) {
			afficherRec(noeud.children[0]);
		}

		noeud.valeur.afficher();
		
		if (noeud.children[1] != null) {
			afficherRec(noeud.children[1]);
		}
	}
	
	private void incrSolutions() {
		nbSolutions++;
	}
	@Override
	public void ajouterSolution(Solution solution) {
		System.out.print("ajout ");solution.afficher();
		if(racine != null)	
			ajouterSolutionRec(racine, solution);
		else{
			solution.table = Arrays.copyOf(solution.table, solution.table.length);
			racine = new NoeudABR(solution);
			incrSolutions();
		}
			
	}

	private void ajouterSolutionRec(NoeudABR noeud, Solution solution) {
		int compare = solution.compareTo(noeud.valeur);
		if(compare == 1) {
			if(noeud.children[1] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[1] = new NoeudABR(solution);
				incrSolutions();
			}
			else
				ajouterSolutionRec(noeud.children[1], solution);
		}
		else if(compare != 0) {
			if(noeud.children[0] == null) {
				solution.table = Arrays.copyOf(solution.table, solution.table.length);
				noeud.children[0] = new NoeudABR(solution);
				incrSolutions();
			}
			else
				ajouterSolutionRec(noeud.children[0], solution);
		}
	}
	
	@Override
	public int nombreSolutions() {
		return nbSolutions;
	}

}
