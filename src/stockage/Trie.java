package stockage;

import main.Solution;
import utils.Helper;


public class Trie implements ISolution{
	private int nbSolutions;
	private static TrieNode root;
	static final int BINARY_SIZE = 2;
	private int nbVar;
	public Trie(int n) {
		root = new TrieNode();
		this.nbVar = n;
		nbSolutions = 0;
	}
	
	static class TrieNode  { 
        TrieNode[] children = new TrieNode[BINARY_SIZE]; 
        TrieNode(){ 
            for (int i = 0; i < BINARY_SIZE; i++) 
                children[i] = null; 
        } 
        
        public void afficherRec(boolean[] buff, int level, int max) {
    		if ( level == max ) {
    			Helper.afficherSolution(buff);
    			return;
    		}
    		for(int i = 0 ; i < BINARY_SIZE ; i++) {
    			if (children[i] != null) {
    				buff[level] = ((i!=0)?true:false);
    				children[i].afficherRec(buff, level + 1, max);
    			}
    		}
    	}
    }; 
    
    public void ajouterSolution(Solution solution) { 
		System.out.print("ajout ");solution.afficher();
        int length = solution.table.length;  
        TrieNode pCrawl = root; 
        for (int level = 0; level < length; level++) { 
        	boolean index = solution.table[level];
        	int val = (index) ? 1 : 0;
            if (pCrawl.children[val] == null) {
                pCrawl.children[val] = new TrieNode(); 
                if (level == length - 1) {
                	nbSolutions++;
                }
            }
            pCrawl = pCrawl.children[val]; 
        } 
    } 
	
	boolean search(boolean[] solution) { 
        TrieNode pCrawl = root; 
        for (int level = 0; level < nbVar; level++)  { 
        	boolean index = solution[level];
        	int val = (index) ? 1 : 0;
            if (pCrawl.children[val] == null) 
                return false; 
            pCrawl = pCrawl.children[val]; 
        } 
        return (pCrawl != null); 
    } 
	
	public void afficher() {
		System.out.println("Affichage trie :");

		boolean[] buff =  new boolean [nbVar];
	    for(int i = 0 ; i < BINARY_SIZE ; i++) {
	    	if (root.children[i] != null) {
	    		buff[0] = ((i!=0)?true:false);
	    		root.children[i].afficherRec(buff, 1, nbVar);
	    	}
	    }
	}

	@Override
	public int nombreSolutions() {
		return nbSolutions;
	}
}