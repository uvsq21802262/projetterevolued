package stockage;

import main.Formule;
import main.Terme;
import main.Terme.grayStrategy;
import utils.DNFGenerator;
import utils.Helper;

public class Testeur {
	
	
	private Terme.grayStrategy strat;
	private ISolution sol;
	private boolean extension;
	private boolean aleatoire;
	private Formule formule;
	
	public Testeur(boolean aleatoire, int nbVar, int nbMaxTermes, int tailleMinTerme, double probaNot, double probaVar,
			grayStrategy strat, ISolution sol, boolean extension) {
		super();
		this.extension = extension;
		this.strat = strat;
		this.sol = sol;
		this.formule = DNFGenerator.generateDNF2(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, this.aleatoire).addSolutionStrategy(this.sol).addGrayStrategy(this.strat);
	}
	
	public void Tester()
	{
		Helper.afficherListeVar(formule.variables);
		System.out.println("Formule : "+formule.afficher());
		if(extension)
			formule.etendre();
		
		System.out.println("formule etendue : "+formule.afficher());
		
		formule.enleverDoublons();
		System.out.println("Formule classee : "+formule.afficher());
		
		double debut = (System.currentTimeMillis()/1000.0);

		formule.trouverSolutions();
		
		double fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);
		formule.afficherSolutions();
		System.out.println(formule.solutions.nombreSolutions()+ " solutions\nfin.");
		
	}

	public Terme.grayStrategy getStrat() {
		return strat;
	}

	public void setStrat(Terme.grayStrategy strat) {
		this.strat = strat;
	}

	public ISolution getSol() {
		return sol;
	}

	public void setSol(ISolution sol) {
		this.sol = sol;
	}

	public boolean isExtension() {
		return extension;
	}

	public void setExtension(boolean extension) {
		this.extension = extension;
	}

	public boolean isAleatoire() {
		return aleatoire;
	}

	public void setAleatoire(boolean aleatoire) {
		this.aleatoire = aleatoire;
	}
	
	
	
	
	
	
}
