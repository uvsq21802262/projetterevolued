package stockage;

import main.Solution;


public interface ISolution {
	public void afficher();
	
	public void ajouterSolution(Solution solution);
	
	public int nombreSolutions();
	
}
