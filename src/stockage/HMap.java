package stockage;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import utils.Helper;

import main.Solution;

public class HMap implements ISolution {
	private HashMap<Integer, ListeTriee> solutions;
	private int n;
	private int nbSolutions;
	public HMap(int n){
		solutions = new HashMap<Integer, ListeTriee>();
		this.n = n;
		nbSolutions = 0;
	}
	@Override
	public void afficher() {
		System.out.print("Affichage HMAP");

		ListeTriee liste = new ListeTriee();
		List<Integer> keys = new LinkedList<Integer>(solutions.keySet());
		boolean bucket = true;
		if(bucket){
			System.out.println(" par bucket : ");
			for(Integer k : keys){
				System.out.println("Bucket "+k+" : ");
				solutions.get(k).afficher();
				}
		}else{
			System.out.println(" par ordre croissant : ");
			for(Integer k : keys){
				for(Solution s : solutions.get(k).solutions){
					liste.ajouterSolution(s);
				}
			}
			liste.afficher();
		}
		
	}
	@Override
	public int nombreSolutions() {
		return nbSolutions;
	}
	@Override
	public void ajouterSolution(Solution solution) {
		System.out.print("ajout ");solution.afficher();
		int hash = hash(solution);
		Integer i = new Integer(hash);
		if(solutions.keySet().contains(i)){
			//System.out.println("Contient clé : "+hash);
			solutions.get(i).ajouterSolution(solution);
			nbSolutions++;
		}else {
			//System.out.println("Contient pas clé : "+hash);
			solutions.put(i, new ListeTriee());
			solutions.get(i).ajouterSolution(solution);
			nbSolutions++;
		}
	}
	private int hash(Solution sol){
		int i = 0;
		//ne marche pas pour 32 pile -> int pas sur 32 bits?? bit reserve au signe ?
		int jump = 31;
		int cum = 0;
		int t;
		//n doit être inférieur ou égale à 32 et > 0 
		while(i + jump <  n) {
			boolean[] tmp = Arrays.copyOfRange(sol.table, i, i + jump);
			t = Helper.binaireAEntier(tmp);
			cum = cum ^ t;
			i += jump;
		}
		if(i + jump >= n ){
			boolean[] tmp = Arrays.copyOfRange(sol.table, i, n);
			t = Helper.binaireAEntier(tmp);
			cum = cum ^ t;
		}
		return cum;
	}
}
