package main;

public class Link {
	public Variable val;
	public Link next;
	public Link(Variable val, Link next) {
		this.val= val;
		this.next= next;
	}
	public String toString() {
		String s= "[ "+val;
		for (Link cur=next; cur!=null; cur= cur.next) {
		    s= s+" "+ cur.val;
		}
		s= s+" ]";
		return s;
	}
}
