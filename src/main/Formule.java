package main;
import java.util.ArrayList;
import java.util.List;

import stockage.ISolution;



public class Formule {

	private static final String and = " v ";
	public int nbVar;
	public List<Terme> termes;
	public ISolution solutions;
	private boolean etendue;
	public boolean triee;
	public List<Variable> variables;
	private Terme.grayStrategy strategy;
	public Formule(int n) {
		nbVar = n;
		termes = new ArrayList<Terme>();
		//variables = new ArrayList<Variable>();
		etendue = false;
		triee = false;
		
	}
	
	public Formule addSolutionStrategy(ISolution sol){
		this.solutions = sol;
		return this;
	}
	public Formule addGrayStrategy(Terme.grayStrategy strat){
		this.strategy = strat;
		return this;
	}
	public void trouverSolutions() {
		//List<Variable> variables = this.variables;
		if(!etendue) {
			System.out.println("Solution pour formule non étendue");
			for(Terme t:this.termes){
				//System.out.println("terme : "+t);
				t.trouverSolutions(solutions, variables, strategy);
			}
		} else {
			System.out.println("Solution pour formule étendue");
			for(Terme t:this.termes){
				solutions.ajouterSolution(new Solution(t.solutionFixe(variables)));
			}
		}
	}
	
	public void ajouterTerme(Terme terme) {
		termes.add(terme);
	}
	
	public void afficherSolutions(){
		solutions.afficher();
	}
	
	public String afficher() {
		String res = "   ";
		for(Terme t : termes) {
			res+= t.toString() + and;
		}
		return res.substring(0, res.length() - and.length());
	}
	
	//on suppose que les termes sont triés
	public void etendre() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		for (Terme t : termes) {
			List<Variable> vars = new ArrayList<Variable>();
			vars.addAll(this.variables);
			vars.removeAll(t.variables());
			nvTermes.addAll(t.etendre(vars));
		}
		etendue = true;
		termes = nvTermes;
	}
	
	public void enleverDoublons() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		if(triee) {
			System.out.println("formule deja triee");
			for (Terme t : termes) {
				System.out.print("Analyse du terme  "+t);
				if(!t.estDansListe(nvTermes)) {
					System.out.println(" -> "+t);
					nvTermes.add(t);
				}else {
					System.out.println(" : suppression doublon ");
				}
			}
			termes = nvTermes;
			return;
		}
		for (Terme t : termes) {
			System.out.print("Analyse du terme  "+t);
			t.trierTerme();
			if(!t.estDansListe(nvTermes)) {
				System.out.println(" -> "+t);
				nvTermes.add(t);
			}else {
				System.out.println(" : suppression doublon ");
			}
		}
		termes = nvTermes;
	}
	
}
