package main;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import stockage.ISolution;
import utils.Helper;


public class Terme {
	public enum grayStrategy {ITER1, ITER2, REC};
	private static final String not = "¬";

	public List<Variable> positifs;
	public List<Variable> negatifs;

	public Terme() {
		positifs = new ArrayList<Variable>();
		negatifs = new ArrayList<Variable>();
	}

	public boolean contient(Variable v) {
		for (int i = 0; i < positifs.size(); i++) {
			if (positifs.get(i) == v)
				return true;
			if (negatifs.get(i) == v)
				return true;
		}
		return false;
	}

	public void ajouterVariable(Variable v, boolean signe) {

		if (contient(v))
			return;
		if (signe) {
			positifs.add(v);
			negatifs.add(null);
		} else {
			positifs.add(null);
			negatifs.add(v);
		}
	}

	public int taille() {
		return positifs.size();
	}

	public String toString() {
		String res = "";
		for (int i = 0; i < positifs.size(); i++) {
			if (positifs.get(i) == null) {
				res += not + negatifs.get(i).toString();
			} else {
				res += positifs.get(i).toString();
			}
		}
		return res;
	}

	public List<Terme> etendre(List<Variable> variables) {
		System.out.println("on etend " + this);
		// System.out.print("vars dispo : ");
		// Listes.afficherListeVar(variables);
		// il faut trier le terme avant et variables
		List<Terme> termes = new ArrayList<Terme>();

		if (variables.size() != 0) {
			Variable v = variables.remove(0);
			Terme t1 = this.clone();
			Terme t2 = this.clone();
			// id = la place de v dans le terme
			t1.positifs.add(v.id, v);
			t1.negatifs.add(v.id, null);
			t2.positifs.add(v.id, null);
			t2.negatifs.add(v.id, v);
			termes.addAll(t1.etendre(variables));
			termes.addAll(t2.etendre(variables));
		} else {
			termes.add(this);
		}
		return termes;
	}
	//les var qui apparaissent uniquement
	public List<Variable> variables() {
		List<Variable> variables = new ArrayList<>();
		for (int i = 0; i < positifs.size(); i++) {
			Variable v;
			if (positifs.get(i) == null) {
				v = negatifs.get(i);
			} else {
				v = positifs.get(i);
			}
			variables.add(v);
		}
		return variables;
	}

	private void triFusion(List<Variable> variables, int d, int f) {
		if (f - d == 0)
			return;
		int milieu = (d + f) / 2;
		triFusion(variables, d, milieu);
		triFusion(variables, milieu + 1, f);
		fusion(variables, d, milieu, f);
	}

	private void fusion(List<Variable> variables, int d1, int f1, int f2) {
		int d2 = f1 + 1;
		Variable tmp[] = new Variable[f1 - d1 + 1];
		boolean signe[] = new boolean[f1 - d1 + 1];
		for (int i = d1; i <= f1; i++) {
			tmp[i - d1] = variables.get(i);
			signe[i - d1] = (positifs.get(i) != null);
		}

		int compt1 = d1;
		int compt2 = d2;

		for (int i = d1; i <= f2; i++) {
			if (compt1 == d2) { // c'est que tous les éléments du premier
								// tableau ont été utilisés
				break; // tous les éléments ont donc été classés
			} else if (compt2 == (f2 + 1)) {// c'est que tous les éléments du
											// second tableau ont été utilisés
				variables.set(i, tmp[compt1 - d1]); // on ajoute les éléments
													// restants du premier
													// tableau
				if (signe[compt1 - d1]) {
					positifs.set(i, tmp[compt1 - d1]);
					negatifs.set(i, null);
				} else {
					negatifs.set(i, tmp[compt1 - d1]);
					positifs.set(i, null);
				}
				compt1++;
			} else if (tmp[compt1 - d1].id < variables.get(compt2).id) {
				Variable var = tmp[compt1 - d1];
				variables.set(i, var); // on ajoute un élément du premier
										// tableau
				if (signe[compt1 - d1]) {
					positifs.set(i, var);
					negatifs.set(i, null);
				} else {
					negatifs.set(i, var);
					positifs.set(i, null);
				}
				compt1++;
			} else {
				variables.set(i, variables.get(compt2)); // on ajoute un élément
															// du second tableau
				positifs.set(i, positifs.get(compt2));
				negatifs.set(i, negatifs.get(compt2));
				compt2++;
			}
		}
	}

	public Terme clone() {
		Terme t = new Terme();
		t.positifs.addAll(this.positifs);
		t.negatifs.addAll(this.negatifs);
		return t;
	}

	public void trierTerme() {
		List<Variable> variables = variables();
		// Collections.sort(variables);
		triFusion(variables, 0, variables.size() - 1);
	}

	public boolean equals(Terme t) {
		int n = t.taille();
		if (this.taille() != n)
			return false;
		for (int i = 0; i < n; i++) {
			if (t.positifs.get(i) != this.positifs.get(i)) {
				return false;
			}
		}
		return true;
	}

	public boolean estDansListe(List<Terme> termes) {
		for (Terme t : termes) {
			if (t.equals(this))
				return true;
		}
		return false;
	}

	public boolean[] solutionFixe(List<Variable> variables) {
		int n = variables.size();
		boolean[] sol = new boolean[n];
		for (int i = 0; i < this.positifs.size(); i++) {
			Variable v = this.positifs.get(i);
			if (v == null) {
				v = this.negatifs.get(i);
				sol[v.id] = false;
			} else {
				sol[v.id] = true;
			}
		}
		return sol;
	}

	public void solutionIter1(ISolution solutions, boolean[] table, List<Variable> variables) {
		int tete = 0;
		int n = (int)Math.pow(2, variables.size());
		int[] indices = new int[n - 1];
		//stocke la reference, lorsquon sur de lajout on fait une copie du tableau dans le classe qui implemente ISolution
		solutions.ajouterSolution(new Solution(table));//Arrays.copyOf(table, table.length)));
		int size = variables.size();
		for(int i = 0; i < size; i++) {
			int ind = variables.remove(variables.size()-1).id;////revoir le type de listela liste en etree doit être triee
			table[ind] = true;//inversion du plus grand bit
			solutions.ajouterSolution(new Solution(Arrays.copyOf(table, table.length)));
			indices[tete] = ind;
			if(tete > 0){
				for(int j = (tete - 1) ; j >= 0 ; j--) {
					int i2 = indices[j];
					table[i2] = !table[i2];
					solutions.ajouterSolution(new Solution(table));//Arrays.copyOf(table, table.length)));
					tete++;
					indices[tete] = i2;
				}
			}
			tete++;
		}
		
	}
	public void solutionIter2(ISolution solutions, boolean[] table, Link vars) {
        Stack<Task> tasks= new Stack<Task>();
        tasks.push(new Task(vars, false));
        while (!tasks.empty()) {
            Task task= tasks.pop();
            vars= task.variables;
            boolean invert= task.invert;
            if (task.nextPos!= -1) {
                table[task.nextPos]= task.val;
            }
            if(vars != null) {
            	Variable v = vars.val;
            	vars = vars.next;
                int nextPos = v.id;
                if(invert){
                    Task t1= new Task(nextPos, true, vars, false);
                    Task t2= new Task(nextPos, false, vars, true);
                    tasks.push(t2);
                    tasks.push(t1);
                }else{
                    Task t1= new Task(nextPos, false, vars, false);
                    Task t2= new Task(nextPos, true, vars, true);
                    tasks.push(t2);
                    tasks.push(t1);
                }
            }else{
                //Helper.afficherSolution(table);
                solutions.ajouterSolution(new Solution((Arrays.copyOf(table, table.length))));
            }                   
        }   
    }
//	//variables toutes les variables de la formule
	public void trouverSolutions(ISolution solutions, List<Variable> variables, grayStrategy strategy) {

        System.out.println("Analyse du terme "+this);
		boolean[] table = solutionFixe(variables);
		if(strategy == grayStrategy.ITER1){
			List<Variable> vars = new ArrayList<Variable>();
			vars.addAll(variables);
			vars.removeAll(this.variables());
			solutionIter1(solutions, table, vars);
			
			return;
		}
		List<Variable> varsUtilisees = this.variables();
		Link vars = null;
		for(Variable v : variables) {
			if(!varsUtilisees.contains(v)) {
				vars = new Link(v, vars);//liste chainee
			}
		}
		//System.out.println(this + " "+(int)Math.pow(2, vars.size()));
		if(strategy == grayStrategy.ITER2){
			//System.out.println("Code de Gray iteratif1");
			//solutionIter2(solutions, table, vars);
		}
		else {
			//System.out.println("Code de Gray recursif");
			solutionRec(solutions, table, vars, false);
		}
	}


	public void solutionRec(ISolution solutions, boolean[] table, Link vars, boolean invert){
		if(vars != null) {
			int nextPos = vars.val.id;
			vars = vars.next;
			if(invert){
				table[nextPos] = true;
				solutionRec(solutions, table, vars, false);
				table[nextPos] = false;
				solutionRec(solutions, table, vars, true);
			}else{
				table[nextPos] = false;
				solutionRec(solutions, table, vars, false);
				table[nextPos] = true;
				solutionRec(solutions, table, vars, true);
			}
		}else{
			//Listes.afficherSolution(table);
			solutions.ajouterSolution(new Solution((Arrays.copyOf(table, table.length))));
			
		}
		
	}
}
