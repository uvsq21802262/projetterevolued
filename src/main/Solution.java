package main;

import utils.Helper;


public class Solution implements Comparable<Solution>{
	public boolean[] table;
	
	public Solution(boolean[] sol) {
		// table n'est qu'une REFERENce à l'initialisation il faudra a lajout (dans ISolution) faire une copie
		table = sol;
	}
	public void afficher() {
		String s = "";
		for(int i = 0; i<table.length;i++) {
			s+=((table[i])?1:0);
		}
		System.out.println(s + ((table.length > 32)?"":" : " + Helper.binaireAEntier(table)));
	}
	@Override
	public int compareTo(Solution arg0) {
		//System.out.println("comparaison de "); this.afficher();
		//arg0.afficher();
		int i =  0;
		while (i != this.table.length) {
			if (this.table[i] && !arg0.table[i])
				return 1;
			if (!this.table[i] && arg0.table[i])
				return -1;
			i++;
		}
		return 0;
	}
}
