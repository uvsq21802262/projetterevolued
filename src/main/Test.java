package main;

import stockage.ABR;
import stockage.HMap;
import stockage.ISolution;
import stockage.Liste;
import stockage.ListeTriee;
import stockage.Trie;
import utils.DNFGenerator;
import utils.Helper;

public class Test {

	public static void main(String [] args) {
		//si aleatoire = true alors les variables apparaitront dans un ordre aleatoire sinon dans l'ordre
		boolean aleatoire = true;
		//nb de variables differentes dans la formule
		int nbVar = 3;
		//nb de termes maximum dans la formule
		int nbMaxTermes = 3;
		//taille minimum du terme ( en variables )
		int tailleMinTerme = 1;
		//proba d'aparition qu"un variable soit negative
		double probaNot = 0.3;
		//proba d'aparition qu"une variable apparraisse dans chaque terme
		double probaVar = 0.5;
		
		Terme.grayStrategy strat = Terme.grayStrategy.ITER1;

		//strat = Terme.grayStrategy.ITER2;
		//strat = Terme.grayStrategy.REC;
		
		//structure qu'on va utiliser pour stocker la solution : soit trie, soit liste
		ISolution sol;
		//sol= new Trie(nbVar);
		//sol = new Liste();
		//sol = new ListeTriee();
		sol= new ABR();
		//sol = new HMap(nbVar);
		boolean extension = false;
		//Formule formule = DNFGenerator.formuleTest().addSolutionStrategy(sol);
		Formule formule = DNFGenerator.generateDNF2(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, aleatoire).addSolutionStrategy(sol).addGrayStrategy(strat);
		Helper.afficherListeVar(formule.variables);
		System.out.println("Formule : "+formule.afficher());
		if(extension)
			formule.etendre();
		
		System.out.println("formule etendue : "+formule.afficher());
		
		formule.enleverDoublons();
		System.out.println("Formule classee : "+formule.afficher());
		
		double debut = (System.currentTimeMillis()/1000.0);

		formule.trouverSolutions();
		
		double fin = (System.currentTimeMillis()/1000.0);
		System.out.println(fin - debut);
		formule.afficherSolutions();
		System.out.println(formule.solutions.nombreSolutions()+ " solutions\nfin.");
		//System.out.println( (int)Math.pow(2, nbVar) );
	}
}
